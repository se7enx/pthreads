<?php

namespace ContextualCode\Threads;

use Volatile;

/**
 * Adds tasks to pool and runs it
 *
 * @author Serhey Dolgushev <serhey@contextualcode.com>
 */
final class Runner
{
    private $pool = null;
    private $data = null;

    public function __construct(int $poolSize = 0)
    {
        $this->data = new Volatile();
        $this->pool = new Pool(
            $poolSize,
            Worker::class,
            [$this->data]
        );
    }

    public function addTask(Thread $task): void
    {
        $this->pool->submit($task);
    }

    public function process(): array
    {
        $this->pool->process();

        return (array) $this->data;
    }
}