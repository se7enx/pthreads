<?php

namespace ContextualCode\Threads;

use Thread as BaseThread;

/**
 * Extends Thread class from pthreads PHP extension
 *
 * @author Serhey Dolgushev <serhey@contextualcode.com>
 */
class Thread extends BaseThread
{
    public function run(): void
    {
        $result = $this->do();
        $this->saveResults($result);
    }

    public function do()
    {
        return null;
    }

    protected function saveResults($jobResult): void
    {
        $this->worker->data[] = $jobResult;
    }
}
